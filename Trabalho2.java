import javax.swing.*;
import java.util.*;

public class Trabalho2 {

	static class Juckebox {
		String A, C, M;
		int N;
	}

	static Juckebox J[] = new Juckebox[12];
	static int troca, aux;
	static String auxm;
	static int i, cont_1, soma;
	static int cont_2, cont_3, amenos;
	static int media, fim;
	static int mais_i, menos_i;
	static String concatenar = "";
	static String artista = "";

	public static void vetores() {

		for (i = 0; i < 12; i++) {

			J[i] = new Juckebox();
		}

		J[0].A = "Pearl Jam";
		J[1].A = "Pearl Jam";
		J[2].A = "Alice in Chains";
		J[3].A = "Alice in Chains";
		J[4].A = "Foo Fighters";
		J[5].A = "Foo Fighters";
		J[6].A = "Rihanna";
		J[7].A = "Rihanna";
		J[8].A = "Rihanna";
		J[9].A = "Beyonce";
		J[10].A = "Beyonce";
		J[11].A = "Beyonce";

		J[0].M = "Alive";
		J[1].M = "State of Love and Trust";
		J[2].M = "Would I?";
		J[3].M = "Man in the box";
		J[4].M = "Rope";
		J[5].M = "Saint Cecilia";
		J[6].M = "Skin";
		J[7].M = "Needed Me";
		J[8].M = "Work";
		J[9].M = "Crazy in Love";
		J[10].M = "Love On Top";
		J[11].M = "Sweet Dreams";

		J[0].C = "101";
		J[1].C = "102";
		J[2].C = "103";
		J[3].C = "201";
		J[4].C = "202";
		J[5].C = "203";
		J[6].C = "301";
		J[7].C = "302";
		J[8].C = "303";
		J[9].C = "401";
		J[10].C = "402";
		J[11].C = "403";
	}

	// 1) (PROCEDIMENTO e passagem de parâmetro por REFERÊNCIA) Solicite ao
	// usuário o número de vezes
	// que a música foi pedida(TOCADA), a mensagem que aparece para o usuário
	// deve seguir o seguinte modelo:
	// "Olá, insira o número de vezes que a música (Nome da Música) de (Nome do
	// Artista) foi pedida nesta JUKEBOX: "

	public static void opcao_1() {
		vetores();
		for (i = 0; i < 12; i++) {
			J[i].N = Integer.parseInt(JOptionPane.showInputDialog("Olá, insira o número de vezes que a música " + J[i].M
					+ " de " + J[i].A + " foi pedida nesta JUKEBOX: "));
		}
	}

	// 2) (FUNÇÃO e passagem de parâmetro por VALOR) Solicite ao usuário que
	// informe um nome de um dos artista
	// (STRING) e utilizando PESQUISA LINEAR mostre o número de músicas que este
	// artista possui cadastrado no sistema da JUKEBOX.

	public static String opcao_2() {
		String nomeDoArtista = JOptionPane.showInputDialog("Digite o nome do Artista: ");
		return pesquisaLinearPorArtista(nomeDoArtista);
	}

	// 2) (FUNÇÃO e passagem de parâmetro por VALOR) Solicite ao usuário que
	// informe um nome de um dos artista
	// (STRING) e utilizando PESQUISA LINEAR mostre o número de músicas que este
	// artista possui cadastrado no sistema da JUKEBOX.
	public static String pesquisaLinearPorArtista(String nomeDoArtista) {
		cont_1 = 0;

		for (i = 0; i < 12; i++) {
			if (nomeDoArtista.equalsIgnoreCase(J[i].A)) {
				cont_1++;
			}

		}
		JOptionPane.showMessageDialog(null,
				"O número de músicas que " + nomeDoArtista + " possui cadastrado no sistema é: " + cont_1);
		return artista;
	}

	// 3) (FUNÇÃO e passagem de parâmetro por VALOR) Mostre quantas vezes a
	// máquina tocou
	// músicas. Como cada música custa a moeda de R$ 1,00 , o dono deseja saber
	// quantos reais tem em caixa.

	public static int opcao_3() {
		return numeroDeMusicastocadas(soma);
	}

	public static int numeroDeMusicastocadas(int soma) {

		soma = 0;

		for (i = 0; i < 12; i++) {

			soma = soma + J[i].N;

		}
		JOptionPane.showMessageDialog(null,
				"A máquina tocou músicas " + soma + " vezes e possui R$" + soma + " em caixa.");

		return soma;
	}

	// 4) (FUNÇÃO e passagem de parâmetro por VALOR) Mostre o nome do artista, o
	// número do
	// compartimento e a música MAIS PEDIDA.

	public static int opcao_4() {

		return musicaMaisTocada(mais_i);

	}

	public static int musicaMaisTocada(int mais_i) {
		mais_i = 0;
		for (i = 0; i < 12; i++) {
			if (J[i].N > J[mais_i].N) {
				mais_i = i;

			}

		}
		JOptionPane.showMessageDialog(null, "A música mais tocada é " + J[mais_i].M + "do artista " + J[mais_i].A
				+ " localizada " + "no compartimento " + J[mais_i].C + ".");
		return mais_i;
	}

	// 5) (FUNÇÃO e passagem de parâmetro por VALOR) Mostre o número do
	// compartimento e o
	// nome do artista da música MENOS pedida.

	public static int opcao_5() {
		return musicaMenosTocada(menos_i);
	}

	public static int musicaMenosTocada(int menos_i) {
		menos_i = 0;

		for (i = 0; i < 12; i++) {
			if (J[i].N < J[menos_i].N) {
				menos_i = i;

			}

		}
		JOptionPane.showMessageDialog(null, "O artista que teve a música menos pedida é do(a) artista " + J[menos_i].A
				+ " e a mesma se encontra no compartimento " + J[menos_i].C + ".");
		return menos_i;
	}

	// 6) (FUNÇÃO e passagem de parâmetro por VALOR) Solicite que o usuário
	// informe um número
	// do compartimento(STRING), mostre o nome do artista, nome da música e
	// número de vezes
	// que tocou.

	public static int opcao_6() {
		String compart = JOptionPane.showInputDialog("Digite o número de um compartimento: ");
		return artistaProcurado(compart);
	}

	public static int artistaProcurado(String compart) {
		int proc = 0;
		for (i = 0; i < 12; i++) {
			if (compart.equalsIgnoreCase(J[i].C)) {
				proc = i;
			}
		}
		JOptionPane.showMessageDialog(null, "O compartimento " + J[proc].C + ", contém a música " + J[proc].M
				+ " do artista " + J[proc].A + " e a mesma foi tocada " + J[proc].N + " vezes.");
		return proc;

	}

	// 7) (PROCEDIMENTO e passagem de parâmetro por REFERÊNCIA)Mostre as 10
	// músicas mais tocadas.
	// Em uma única mensagem, mostre as 10 músicas ordenadas por número de
	// pedidos(número
	// de vezes que foram tocadas). Para isso coloque todas as leituras em um
	// VETOR e ordene com
	// o BubbleSort.

	public static void opcao_7() {
		troca = 1;
		fim = 11;

		while (troca == 1) {
			troca = 0;

			for (i = 0; i < fim; i++) {
				if (J[i].N < J[i + 1].N) {

					aux = J[i].N;
					J[i].N = J[i + 1].N;
					J[i + 1].N = aux;
					auxm = J[i].M;
					J[i].M = J[i + 1].M;
					J[i + 1].M = auxm;
					troca = 1;

				}
			}

			fim = fim - 1;

		}

		i = 2;

		for (i = 0; i < 10; i++) {
			concatenar += J[i].M + "\n";

		}
		JOptionPane.showMessageDialog(null, "As 10 músicas mais tocadas foram: " + concatenar);
	}

	// 8) (PROCEDIMENTO e passagem de parâmetro por REFERÊNCIA)Mostre o número
	// de músicas
	// que tocaram mais vezes que a média de músicas tocadas.

	public static void opcao_8() {
		cont_1 = 0;
		media = 0;

		soma = 0;

		for (i = 0; i < 12; i++) {

			soma = soma + J[i].N;
		}

		media = soma / 12;

		for (i = 0; i < 12; i++) {
			if (J[i].N > media) {
				cont_1++;
			}
		}
		JOptionPane.showMessageDialog(null,
				"O número de músicas que tocaram mais vezes que a média é: " + cont_1 + ".");

	}

	// 9) (FUNÇÃO e passagem de parâmetro por VALOR) Solicite que o usuário
	// informe o nome de
	// um artista, mostre o número de vezes que este ARTISTA foi tocado nesta
	// JUKEBOX, incluindo a
	// soma de todas as músicas.

	public static int opcao_9() {
		String artistaProcurado = JOptionPane.showInputDialog("Digite o nome de um artista");
		return musicasDoArtista(artistaProcurado);
	}

	public static int musicasDoArtista(String artistaProcurado) {
		cont_2 = 0;

		soma = 0;

		for (i = 0; i < 12; i++) {

			if (artistaProcurado.equalsIgnoreCase(J[i].A)) {

				cont_2 = cont_2 + J[i].N;

			}

		}

		cont_3 = 0;

		for (i = 0; i < 12; i++) {

			if (artistaProcurado.equalsIgnoreCase(J[i].A)) {

				cont_3++;

			}

		}
		JOptionPane.showMessageDialog(null, "O " + artistaProcurado + " foi tocado " + cont_2 + " vezes " + " e possui "
				+ cont_3 + " músicas cadastradas.");
		return cont_2;
	}

	// 10) (PROCEDIMENTO e passagem de parâmetro por REFERÊNCIA)Considerando os
	// compartimentos com final 2 (102,202,302,402), mostre o nome do música
	// mais tocada.

	public static void opcao_10() {
		mais_i = 0;

		for (i = 1; i < 12; i++) {

			if (J[i].N > J[mais_i].N) {

				mais_i = i + 3;

			}

		}
		JOptionPane.showMessageDialog(null,
				"A música mais tocada nos compartimentos com final 2 é " + J[mais_i].M + ".");
	}

	public static void main(String args[]) {
		int opcao = 1;
		while (opcao > 0 && opcao < 12) {
			opcao = Integer.parseInt(JOptionPane.showInputDialog("MENU: \n\n"
					+ "1) Solicite ao usuário o número de vezes que a música foi pedida(TOCADA);\n"
					+ "2) Informe um nome de um dos artista para saber o número de músicas que este artista possui cadastrado ;\n"
					+ "3) Quantas músicas foram tocadas e qual o valor em caixa;\n" + "4) Música mais pedida;\n"
					+ "5) Música menos pedida;\n"
					+ "6) Informe um compartimento e saiba o nome do artista, a música e o número de vezes que ela foi tocada;\n"
					+ "7) As 10 músicas mais tocadas;\n" + "8) O número de musicas que tocaram mais do que a média;\n"
					+ "9) Informe o nome de um artista e saiba quantas vezes ele foi tocado e quantas músicas ele possui;\n"
					+ "10) Saiba qual é a música mais tocada nos compartimentos com final 2;\n" + "11) Sair"));

			if (opcao == 1) {
				opcao_1();

			} else if (opcao == 2) {
				opcao_2();

			} else if (opcao == 3) {
				opcao_3();

			} else if (opcao == 4) {
				opcao_4();

			} else if (opcao == 5) {
				opcao_5();

			} else if (opcao == 6) {
				opcao_6();

			} else if (opcao == 7) {
				opcao_7();

			} else if (opcao == 8) {
				opcao_8();

			} else if (opcao == 9) {
				opcao_9();

			} else if (opcao == 10) {
				opcao_10();

			} else {
				opcao = 0;
			}

		}
	}
}
